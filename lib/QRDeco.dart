import 'package:flutter/material.dart';
import 'package:naca/models/Event.dart';
import 'package:naca/primaryButton.dart';
import 'package:naca/secondaryButton.dart';

import 'utils.dart';
import 'QR.dart';

/// Wrapper du QR-code d'un événement. Il permet d'afficher son nom et son QR, mais il permet
/// également de signaler l'événement passé en paramètre.
class QRDeco extends StatefulWidget {
  final Event event;

  const QRDeco({
    @required this.event,
  });

  @override
  _QRDecoState createState() => _QRDecoState();
}

class _QRDecoState extends State<QRDeco> with TickerProviderStateMixin {
  bool initInfected;
  // Variable indiquant l'écran à afficher
  Screen currentScreen = Screen.qr;

  // Variable indiquant la variante QR à afficher
  bool qrDisplayAlt = false;

  // Contrôleur et variable d'animation pour le changement de couleur
  AnimationController _controller;
  Animation<Color> animation;

  @override
  void initState() {
    super.initState();

    // Définition du contrôleur qui sera utilisé pour le changement de couleur
    _controller = AnimationController(
      duration: const Duration(milliseconds: 300),
      vsync: this,
    );
    // Initialisation de la variable indiquant si l'événement était déjà infecté à la création du Widget
    initInfected = widget.event.infected;
  }

  @override
  dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // Définition de l'animation de transition de couleurs, du thème au rouge.
    // Devait être dans initState, mais on a besoin du contexte pour la couleur du thème
    animation = ColorTween(
      begin: Theme.of(context).primaryColor,
      end: Colors.red,
    ).animate(_controller)
      ..addListener(() {
        setState(() {});
      });

    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Expanded(
          // Le SafeArea assure que les éléments ne dépassent pas le haut de l'écran (ne se place pas sous la status bar)
          child: SafeArea(
            child: Container(
              margin: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.02),
              // Le Stack permet de placer l'image du virus derrière le QR
              child: Stack(
                alignment: Alignment.center,
                children: [
                  // Fait apparaître et disparaître l'image du virus selon le display du QR choisi
                  AnimatedOpacity(
                    duration: Duration(milliseconds: 300),
                    curve: Curves.easeInOut,
                    opacity:
                        currentScreen == Screen.qr && !qrDisplayAlt ? 1.0 : 0.0,
                    child: Image(
                      image: AssetImage("./assets/VirusSansQR-v10.png"),
                    ),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      // Proportions pour l'espace vide
                      Spacer(),
                      // Proportions pour QR et titre de l'event
                      Expanded(
                        flex: 7,
                        child: AnimatedSwitcher(
                          switchOutCurve: Curves.easeInOutCubic,
                          switchInCurve: Curves.fastLinearToSlowEaseIn,
                          // On a besoin de redéfinir le layout, car c'est un Stack par défaut
                          // et la hauteur des éléments n'est plus adaptée à cause de la valeur par défaut de fit
                          layoutBuilder: (Widget currentChild,
                              List<Widget> previousChildren) {
                            List<Widget> children = previousChildren;
                            if (currentChild != null)
                              children = children.toList()..add(currentChild);
                            return Stack(
                              fit: StackFit
                                  .expand, // Voilà la ligne ajoutée pour que la hauteur soit correcte
                              children: children,
                              alignment: Alignment.center,
                            );
                          },
                          // Transition jouant avec l'opacité et la taille des éléments
                          transitionBuilder:
                              (Widget child, Animation<double> animation) {
                            return FadeTransition(
                              opacity: animation,
                              child: ScaleTransition(
                                scale: animation,
                                child: child,
                              ),
                            );
                          },
                          duration: Duration(milliseconds: 300),
                          child: _getScreen(),
                        ),
                      ),
                      // Proportions pour l'espace avec le bouton pour accéder aux options de signalement
                      Expanded(
                        flex: 1,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            // Bouton
                            Padding(
                              padding: EdgeInsets.only(
                                  top:
                                      MediaQuery.of(context).size.height * 0.1),
                              child: FlatButton(
                                onPressed: () {
                                  setState(() {
                                    // Si l'écran courant est le QR, alors on peut accéder aux écrans de signalement
                                    if (currentScreen == Screen.qr) {
                                      currentScreen = widget.event.infected
                                          ? Screen.eventSignaled
                                          : Screen.signalButton;
                                      // Sinon on affiche le QR
                                    } else
                                      currentScreen = Screen.qr;
                                  });
                                },
                                child: Image.asset(
                                  widget.event.infected
                                      ? "./assets/triple_dots_red.png"
                                      : "./assets/triple_dots_blue.png",
                                  scale: 10,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        // Titre de l'événement
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 70,
          ),
          child: Text(
            _getShortenedText(widget.event.name),
            style: TextStyle(
              fontSize: _getSizeFromText(widget.event.name),
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }

  /// Méthode retournant le widget de l'écran actuel
  Widget _getScreen() {
    switch (currentScreen) {
      case Screen.qr:
        return _buildQRScreen();
        break;
      case Screen.signalButton:
        return _buildSignalButtonScreen();
        break;
      case Screen.confirmation:
        return _buildConfirmationScreen();
        break;
      case Screen.loading:
        return _buildLoadingScreen();
        break;
      case Screen.error:
        return _buildErrorScreen();
        break;
      case Screen.eventSignaled:
        return _buildEventSignaledScreen();
        break;
      default:
        return _buildQRScreen();
    }
  }

  /// Méthode construisant le QR
  Widget _buildQRScreen() {
    return GestureDetector(
      onTap: () {
        setState(() {
          qrDisplayAlt = !qrDisplayAlt;
        });
      },
      // Switch entre les 2 affichages du QR
      child: AnimatedSwitcher(
        child: qrDisplayAlt
            // QR simple bleu foncé qui prend toutes la place disponible
            ? Container(
                key: Key('altQR'),
                child: QR.fromEvent(
                  widget.event,
                  qrColor: qrDisplayAlt
                      ? Theme.of(context).primaryColor
                      : Theme.of(context).primaryColorLight,
                ),
              )
            // QR clair avec des espaces sur les côtés pour adapter à la taille de la bulle du virus en fond
            : Row(
                key: Key('virusQR'),
                children: [
                  Spacer(),
                  Expanded(
                    flex: 10,
                    child: Container(
                      child: QR.fromEvent(
                        widget.event,
                        qrColor: qrDisplayAlt
                            ? Theme.of(context).primaryColor
                            : Theme.of(context).primaryColorLight,
                      ),
                    ),
                  ),
                  Spacer(),
                ],
              ),
        duration: Duration(milliseconds: 300),
        switchOutCurve: Curves.easeInOutCubic,
        switchInCurve: Curves.fastLinearToSlowEaseIn,
        // Animation simple jouant avec l'opacité et la taille
        transitionBuilder: (Widget child, Animation<double> animation) =>
            FadeTransition(
          opacity: animation,
          child: ScaleTransition(
            scale: animation,
            child: child,
          ),
        ),
      ),
    );
  }

  /// Méthode construisant le bouton pour signaler l'événement
  Widget _buildSignalButtonScreen() {
    return PrimaryButton(
      body: "Signaler\ncet événement",
      fontSize: 35,
      onPressed: () {
        setState(() {
          currentScreen = Screen.confirmation;
        });
      },
    );
  }

  /// Méthode construisant l'écran de confirmation de signalement
  Widget _buildConfirmationScreen() {
    return PrimaryButton(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Text(
              "Souhaitez-vous vraiment signaler un cas de covid-19 à cet événement ?",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context).primaryColorLight,
                fontSize: 20,
              ),
            ),
            Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  _customSecondaryButton(
                    body: "Oui",
                    widthPercentage: 0.35,
                    onPressed: _signalEvent,
                  ),
                  _customSecondaryButton(
                    body: "Non",
                    widthPercentage: 0.35,
                    onPressed: () {
                      setState(() {
                        currentScreen = Screen.qr;
                      });
                    },
                  ),
                ],
              ),
            ),
            Text(
              "Cette action est irréversible",
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Theme.of(context).primaryColorLight,
                fontSize: 17,
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// Méthode construisant l'écran de chargement
  Widget _buildLoadingScreen() {
    return PrimaryButton(
      // On utilise la valeur de transition d'animation.
      // Si l'envoi au backend marche, la couleur vire sur le rouge
      backgroundColor: animation.value,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "Signalement de l'événement en cours...",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
              fontSize: 20,
            ),
          ),
          // Widget affichant une roue circulaire de chargement classique
          CircularProgressIndicator(
            valueColor:
                AlwaysStoppedAnimation(Theme.of(context).primaryColorLight),
          ),
        ],
      ),
    );
  }

  /// Méthode construisant l'écran d'erreur
  Widget _buildErrorScreen() {
    return PrimaryButton(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Text(
            "Une erreur est survenue. Voulez-vous réessayer?",
            textAlign: TextAlign.center,
            style: TextStyle(
              color: Theme.of(context).primaryColorLight,
              fontSize: 20,
            ),
          ),
          _customSecondaryButton(
            body: "Réessayer",
            onPressed: _signalEvent,
            widthPercentage: 0.7,
          ),
        ],
      ),
    );
  }

  /// Méthode construisant l'écran à afficher lorsque l'événement a été signalé
  Widget _buildEventSignaledScreen() {
    return PrimaryButton(
      body: "Cet événement a été signalé comme contaminé.",
      // On doit checker avec initInfected pour le cas où l'animation n'a jamais été lancée
      // et la couleur reste sur celle du thème
      backgroundColor: initInfected ? Colors.red : animation.value,
      fontSize: 30,
      onPressed: () {
        setState(() {
          currentScreen = Screen.qr;
        });
      },
    );
  }

  /// Méthode construisant un preset de bouton réutilisable
  Widget _customSecondaryButton(
      {String body, Function onPressed, double widthPercentage}) {
    return Container(
      width: MediaQuery.of(context).size.width * widthPercentage,
      margin: EdgeInsets.symmetric(vertical: 10),
      child: SecondaryButton(
        body: body,
        onPressed: onPressed,
      ),
    );
  }

  /// Méthode signalant l'événement
  void _signalEvent() {
    setState(() {
      // On commence par afficher l'écran de chargement
      currentScreen = Screen.loading;
    });
    // Petit délai pour que les transitions ne soient pas trop brusques
    Future.delayed(const Duration(seconds: 2), () {
      // Signalement de l'événement
      sendCheckCode(widget.event, context)
          // Timeout pour mettre une limite à l'attente
          .timeout(Duration(seconds: 15))
          .then((success) {
        //Si réussite
        setState(() {
          // On passe de l'écran de chargement à l'écran informant que l'événement a bien été signalé
          currentScreen = Screen.eventSignaled;
          // Activation de l'animation de transition de la couleur vers le rouge
          _controller.forward();
          // Mise à jour de l'attribut de l'événement
          widget.event.infected = true;
          widget.event.save();
        });
      }).catchError((e) {
        // S'il y a eu une erreur, comme un timeout par exemple
        print(e);
        setState(() {
          currentScreen = Screen.error;
        });
      });
    });
  }
}

/// Méthode retournant une taille à utiliser pour fontSize selon le nombre de caractères
double _getSizeFromText(String text) {
  if (text.length > 15) {
    return 25;
  } else if (text.length > 10) {
    return 32;
  } else {
    return 45;
  }
}

/// Méthode retournant une chaîne de caractères plus courte si nécessaire
String _getShortenedText(String text) {
  if (text.length > 20)
    return text.substring(0, 15) + "...";
  else
    return text;
}

/// Enumération comprenant tous les écrans affichables pour un événement (QR et signalement)
enum Screen {
  qr,
  signalButton,
  confirmation,
  loading,
  error,
  eventSignaled,
}
