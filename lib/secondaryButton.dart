import 'package:flutter/material.dart';

class SecondaryButton extends StatelessWidget {
  final String body;
  final Function onPressed;
  final double fontSize;
  SecondaryButton({this.body, this.onPressed, this.fontSize = 30});

  @override
  Widget build(BuildContext context) {
    return OutlinedButton(
      onPressed: onPressed,
      child: Text(
        body,
        textAlign: TextAlign.center,
        style: TextStyle(
          color: Theme.of(context).primaryColorLight,
          fontSize: fontSize,
        ),
      ),
      style: ButtonStyle(
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(8.0),
          ),
        ),
        padding: MaterialStateProperty.all(
            EdgeInsets.symmetric(vertical: 6, horizontal: 20)),
        side: MaterialStateProperty.all<BorderSide>(
          BorderSide(
            width: 4,
            color: Theme.of(context).primaryColorLight,
          ),
        ),
      ),
    );
  }
}
