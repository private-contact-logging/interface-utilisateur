import 'package:flutter/material.dart';
import 'package:flutter_sodium/flutter_sodium.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/homepage.dart';
import 'journal.dart';
import 'package:naca/models/Event.dart';
import "package:naca/models/User.dart";
import 'package:intl/date_symbol_data_local.dart';
import 'package:flutter/services.dart';

void main() async {
  // We init the Hive
  await Hive.initFlutter();
  // Since our classes are not native elements, we generate Adapters
  Hive.registerAdapter<Event>(EventAdapter());
  Hive.registerAdapter<User>(UserAdapter());

  await Hive.openBox<Event>('event');
  await Hive.openBox<User>('user');

  Sodium.init(); // Init le package Sodium
  await initUser(); // function to initialize only one user, since we have no login screen...
  await initializeDateFormatting('fr').then((_) => runApp(CovidApp()));
}

class CovidApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // On force ici le mode portrait pour ne pas laisser l'app tourner et creer des overflows.
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      routes: {
        '/journal': (context) => Journal(),
      },
      title: 'Pas encore une application covid-19',
      theme: ThemeData(
        // TODO : réflexion sur l'usage de colorScheme
        primaryColor: const Color(0xFF05668D), //bleu
        accentColor: const Color(0xFF00A896), //vert
        primaryColorLight: const Color(0xFFF0F3BD), //jaune
        secondaryHeaderColor: const Color(0x9900A896), // vert moins opaque
        fontFamily: 'Gabriela',
      ),
      home: HomePage(),
    );
  }
}

Future<void> initUser() async {
  Box<User> userBox = Hive.box<User>('user');
  if (userBox.length == 0) {
    User user = new User(firstname: 'UserFirstName', lastname: 'UserLastName');
    await userBox.put('mainUser', user);
  }
}
