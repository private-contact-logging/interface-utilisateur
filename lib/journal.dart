import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:naca/models/Event.dart';
import 'package:table_calendar/table_calendar.dart';
import 'models/User.dart';
import 'utils.dart';
import 'package:intl/intl.dart';

class Journal extends StatefulWidget {
  Journal({Key key}) : super(key: key);

  @override
  _JournalState createState() => _JournalState();
}

class _JournalState extends State<Journal> with TickerProviderStateMixin {
  Map<DateTime, List> _events;
  List _selectedEvents;
  AnimationController _animationController;
  CalendarController _calendarController;

  @override
  void initState() {
    super.initState();
    final allEvents = Hive.box<Event>('event').values;

    _events = _filterEventsPerDay(allEvents);
    _selectedEvents = _filterEventsToday(allEvents);
    _loadAboutDialogFirstTime();

    _calendarController = CalendarController();
    _animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 400),
    );
    _animationController.forward();
  }

  @override
  void dispose() {
    _animationController.dispose();
    _calendarController.dispose();
    super.dispose();
  }

  void _onDaySelected(DateTime day, List events, List holidays) {
    setState(() {
      _selectedEvents = events;
    });
  }

  void _onVisibleDaysChanged(
      DateTime first, DateTime last, CalendarFormat format) {}

  void _onCalendarCreated(
      DateTime first, DateTime last, CalendarFormat format) {
    verifyCheckCodeForEvents();
  }

  void _onRefreshEvents() {
    verifyCheckCodeForEvents();
    setState(() {});
  }

  // Fonction triant les evenements par jour de creation. Exemple : '17 janvier' -> [event1,event2]
  Map _filterEventsPerDay(Iterable<Event> allEvents) {
    Map<DateTime, List<dynamic>> filteredEvents = new Map<DateTime, List<dynamic>>();
    allEvents.forEach((Event event) {
      var existingDate = filteredEvents.keys.firstWhere(
          (key) => key.isSameDate(event.startDate),
          orElse: () => null);
      if (existingDate != null) {
        filteredEvents[existingDate].add(event);
      } else {
        filteredEvents[event.startDate] = [event];
      }
    });
    return filteredEvents;
  }

  //Fonction permettant de fitrer les evenements pour ceux d'aujourd'hui
  List _filterEventsToday(Iterable<Event> allEvents) {
    List<Event> todaysEvents = new List<Event>();
    allEvents.forEach((Event event) {
      if (event.startDate.isSameDate(DateTime.now())) {
        todaysEvents.add(event);
      }
    });
    return todaysEvents;
  }

  // Premiere fois que l'utilisateur accede au journal, on affiche le dialogue de notation
  void _loadAboutDialogFirstTime() {
    Box<User> userBox = Hive.box<User>('user');
    if (userBox.get('mainUser').seenJournal == false) {
      userBox.get('mainUser').seenJournal = true;
      WidgetsBinding.instance.addPostFrameCallback((_) async {
        await showDialog(
          context: context,
          builder: (BuildContext context) => _buildAboutDialog(context),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          'Journal',
          style: TextStyle(
            color: Theme.of(context).accentColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        backgroundColor: Colors.transparent,
        shadowColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(
            Icons.home,
            color: Colors.black,
          ),
          onPressed: Navigator.of(context).pop,
        ),
        actions: _buildAppBarButtons(),
      ),
      body: Container(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _buildTableCalendar(),
            Expanded(
              child: _buildEventList(),
            ),
          ],
        ),
      ),
    );
  }

  // Simple TableCalendar configuration (using Styles)
  Widget _buildTableCalendar() {
    return TableCalendar(
      locale: 'fr_FR',
      calendarController: _calendarController,
      events: _events,
      initialCalendarFormat: CalendarFormat.week,
      startingDayOfWeek: StartingDayOfWeek.monday,
      calendarStyle: CalendarStyle(
        outsideWeekendStyle:
            TextStyle(color: Theme.of(context).secondaryHeaderColor),
        weekendStyle: TextStyle(color: Theme.of(context).accentColor),
        selectedColor: Theme.of(context).accentColor,
        todayColor: Theme.of(context).secondaryHeaderColor,
        markersColor: Theme.of(context).primaryColor, //Colors.brown[700],
        outsideDaysVisible: false,
      ),
      headerStyle: HeaderStyle(
        formatButtonTextStyle:
            TextStyle().copyWith(color: Colors.white, fontSize: 15.0),
        formatButtonDecoration: BoxDecoration(
          color: Theme.of(context).accentColor,
          borderRadius: BorderRadius.circular(16.0),
        ),
      ),
      daysOfWeekStyle: DaysOfWeekStyle(
          weekendStyle: TextStyle(color: Theme.of(context).accentColor)),
      onDaySelected: _onDaySelected,
      onVisibleDaysChanged: _onVisibleDaysChanged,
      onCalendarCreated: _onCalendarCreated,
    );
  }

  // Build EventList
  Widget _buildEventList() {
    return ListView(
      children: _selectedEvents.map((event) => _eventTile(event)).toList(),
    );
  }

  // Build eventTile - Card
  Widget _eventTile(Event event) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 20.0, vertical: 5.0),
      child: Card(
        shadowColor: Theme.of(context).secondaryHeaderColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15.0),
          side: BorderSide(
              color: Theme.of(context).primaryColorLight, width: 2.0),
        ),
        elevation: 3,
        child: ListTile(
          leading: Icon(
            Icons.event,
            size: 40.0,
            color: Theme.of(context).accentColor,
          ),
          title: Text(
            event.name,
            style: TextStyle(
              fontSize: 30.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          subtitle: Text(
            '${event.location} \n${DateFormat.Hm().format(event.startDate)}${event.endDate != null ? " - " + DateFormat.d().add_MMM().add_Hm().format(event.endDate) : ""}',
            style: TextStyle(
              fontSize: 20.0,
              color: Theme.of(context).primaryColor,
            ),
          ),
          isThreeLine: true,
          trailing: event.infected == true
              ? Icon(
                  Icons.coronavirus_sharp,
                  color: Colors.red,
                  size: 35,
                )
              : Icon(
                  Icons.coronavirus_sharp,
                  color: Colors.green,
                  size: 35,
                ),
          // Fonctionnalite utilise pour debug a ne pas decommenter pour demo
          // onTap: () => sendCheckCode(event, context),
          // onLongPress: () async => await Hive.box<Event>('event')
          //     .deleteAll(Hive.box<Event>('event').keys),
        ),
      ),
    );
  }

  // Popup du début
  Widget _buildAboutDialog(BuildContext context) {
    return new AlertDialog(
      title: const Text('Information d\'événement'),
      content: new Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListTile(
            leading: Icon(Icons.coronavirus_sharp, color: Colors.green),
            title:
                Text('Cet icône indique que l\'événement n\'est pas à risque.'),
          ),
          ListTile(
            leading: Icon(Icons.coronavirus_sharp, color: Colors.red),
            title: Text('Cet icône indique que l\'événement est à risque.'),
          ),
        ],
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.of(context).pop();
          },
          textColor: Theme.of(context).primaryColor,
          child: const Text(
            'J\'ai compris',
            style: TextStyle(
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ],
    );
  }

  List<Widget> _buildAppBarButtons() {
    return <Widget>[
      Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: GestureDetector(
          onTap: () => _onRefreshEvents(),
          child: Icon(
            Icons.refresh,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: GestureDetector(
          onTap: () => showDialog(
            context: context,
            builder: (BuildContext context) => new AlertDialog(
              title: Text('implement export function here'),
            ),
          ),
          child: Icon(
            Icons.import_export,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
      Padding(
        padding: EdgeInsets.only(right: 20.0),
        child: GestureDetector(
          onTap: () => showDialog(
            context: context,
            builder: (BuildContext context) => _buildAboutDialog(context),
          ),
          child: Icon(
            Icons.info,
            color: Theme.of(context).accentColor,
          ),
        ),
      ),
    ];
  }
}
