import 'package:flutter/material.dart';

/// Bouton standard bleu avec police jaune
class PrimaryButton extends StatelessWidget {
  /// Texte à afficher
  final String body;

  final Widget child;

  /// Fonction à utiliser lors d'un click
  final Function onPressed;

  /// Taille de la police
  final double fontSize;

  final Color backgroundColor;

  final EdgeInsets padding;
  PrimaryButton({
    this.body,
    this.child,
    this.onPressed,
    this.fontSize,
    this.padding = const EdgeInsets.only(
      top: 15,
      bottom: 15,
      left: 40,
      right: 40,
    ),
    this.backgroundColor,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      child: OutlinedButton(
        child: body == null
            ? child
            : Text(
                body,
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Theme.of(context).primaryColorLight,
                  fontSize: fontSize,
                ),
              ),
        onPressed: onPressed,
        style: ButtonStyle(
          padding: MaterialStateProperty.all<EdgeInsets>(
            padding,
          ),
          backgroundColor: backgroundColor == null
              ? MaterialStateProperty.all<Color>(Theme.of(context).primaryColor)
              : MaterialStateProperty.all<Color>(backgroundColor),
          shape: MaterialStateProperty.all<RoundedRectangleBorder>(
            RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(8.0),
            ),
          ),
        ),
      ),
    );
  }
}
