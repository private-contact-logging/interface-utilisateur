import 'package:flutter/material.dart';
import 'package:naca/dynamicForm.dart';
import 'package:naca/models/Event.dart';
import 'package:hive/hive.dart';
import 'package:naca/popup.dart';

class CreateEvent extends StatefulWidget {
  @override
  _CreateEventState createState() => _CreateEventState();
}

/// Bouton et texte de création d'événement
class _CreateEventState extends State<CreateEvent> {
  Box<Event> eventCreated;

  @override
  void initState() {
    super.initState();
    eventCreated = Hive.box('event');
  }

  /// Fonction prenant les champs d'un formulaire pour créer un événement dans la bdd locale
  void _addToDB(Map controllers) {
    var event = Event(
      name: controllers['Nom'],
      location: controllers['Lieu'],
      context: controllers['Contexte'],
      startDate: controllers['Date de début'],
      endDate: controllers['Date de fin'],
      infected: false,
      created: true,
    );
    eventCreated.add(event);
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    DateTime now = DateTime.now();
    // Date courante sans l'heure, donc 00:00 pour l'heure
    var currentDate = new DateTime(now.year, now.month, now.day);
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          margin: EdgeInsets.only(top: 50),
          width: 180,
          height: 180,
          child: FloatingActionButton(
            key: Key('createEvent'),
            backgroundColor: Theme.of(context).primaryColor,
            mini: false,
            elevation: 0, //enlever l'ombre
            highlightElevation: 0,
            child: Container(
              padding: EdgeInsets.all(25),
              child: LayoutBuilder(builder: (context, constraint) {
                return Icon(
                  Icons.add,
                  color: Theme.of(context).primaryColorLight,
                  // Rendre l'icône le plus grand possible
                  size: constraint.biggest.height,
                );
              }),
            ),
            // Faire apparaître le popup lors d'un click
            onPressed: () => Popup(
              context: context,
              widget: DynamicForm(
                fields: [
                  ['Nom', String, true],
                  ['Lieu', String, true],
                  ['Contexte', String, true],
                  ['Date de début', DateTime, false, currentDate, false],
                  ['Date de fin', DateTime, false],
                ],
                buttonText: 'Créer',
                onSubmit: _addToDB,
                context: context,
              ),
            ),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1000.0)),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 40,
          ),
          child: Text(
            "Créer un événement",
            style: TextStyle(
              fontSize: 48,
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
