import 'package:flutter/material.dart';

class PersonalQR extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        Container(
          child: Image.asset("./assets/Virus.png"),
        ),
        Container(
          margin: EdgeInsets.only(
            top: 20,
            bottom: 40,
          ),
          child: Text(
            "Mon QR",
            style: TextStyle(
              fontSize: 48,
              color: Theme.of(context).primaryColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      ],
    );
  }
}
