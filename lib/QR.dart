import 'dart:convert';
import 'dart:typed_data';
import 'package:crypto/crypto.dart';
import 'package:convert/convert.dart';
import 'package:flutter/material.dart';
import 'package:naca/models/Event.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:flutter_sodium/flutter_sodium.dart';

class QR extends StatelessWidget {
  //encodage aux autorités

  final String name;
  final String location;
  final String context;
  final String startDate;
  final String endDate;
  final Uint8List notifKey;
  final Uint8List publicKey;
  final Uint8List secretKey;
  final Uint8List salt;
  final String trace;

  final Color qrColor;

  QR._({
    this.name,
    this.location,
    this.context,
    this.startDate,
    this.endDate,
    this.notifKey,
    this.publicKey,
    this.secretKey,
    this.salt,
    this.trace,
    this.qrColor,
  });

  factory QR.fromEvent(Event event, {Color qrColor}) {
    if (event.notifKey.isEmpty) {
      // remplissage de la classe Event lorsqu'elle est pas complète
      event.salt = _salt();
      event.notifKey = _notifkey();
      var seed =
          _getSeed(event.name, event.location, event.salt, event.notifKey);
      KeyPair keyPair = _keyGen(seed);
      event.trace = _getTrace(seed);
      event.publicKey = keyPair.pk;
      event.secretKey = keyPair.sk;
      event.infected = false;
      event.save();
    }
    return QR._(
      name: event.name,
      location: event.location,
      context: event.context,
      startDate: event.startDate.toString(),
      endDate: event.endDate.toString(),
      notifKey: event.notifKey,
      publicKey: event.publicKey,
      secretKey: event.secretKey,
      salt: event.salt,
      trace: event.trace,
      qrColor: qrColor,
    );
  }

  // Salt - utilisé pour mettre de l'aléatoire dans le seed
  static Uint8List _salt() {
    return Sodium.randombytesBuf(32);
  }

  // Clef de notification
  static Uint8List _notifkey() {
    return Sodium.cryptoSecretboxKeygen();
  }

  // Seed - utilisé pour générer les clés secrète et public
  static Map<String, dynamic> _getSeed(name, notifKey, salt, location) {
    return {
      'salt': salt,
      'notifKey': notifKey,
      'name': name,
      'location': location,
    };
  }

  // Génération des clés
  static KeyPair _keyGen(seed) {
    // hashage du seed avec sha256
    Uint8List hashedSeed = Uint8List.fromList(
        sha256.convert(utf8.encode(json.encode(seed))).bytes);
    // Fonction cryptoSignSeedKeypair utilisé dans CrNo.
    // Genère les clés pour signer le paquet qu'on envoie sur le backend
    return Sodium.cryptoSignSeedKeypair(hashedSeed);
  }

  //Json contenant les informations de l'événement
  Map<String, dynamic> _contentQR() {
    return {
      'name': name,
      'location': location,
      'contexte': context,
      'startDate': startDate,
      'endDate': endDate,
      'notificationKey': notifKey,
      'publicKey': publicKey,
    };
  }

  // On wrap le contenu en le signant le seed avec la clé secrète
  String _codeWrapperQR() {
    Map<String, dynamic> content = _contentQR();
    return jsonEncode({
      'signature': CryptoSign.signDetached(
          Uint8List.fromList(utf8.encode(jsonEncode(content))), secretKey),
      'content': content,
      'trace': trace,
    });
  }

  // Rend un checkCode qui permet de vérifier si l'événement est infecté.
  // Le checkCode est envoyé au backend que si l'événement est infécté.
  // On le stocke quand même dans l'événement.
  static String _getTrace(seed) {
    Uint8List authorityPublicKey = Uint8List.fromList(hex.decode(
        'e4d2e06641730ce7c9986b1e7e91bf41bb3b8cc1d76d249fa99d0d8925e87a5c'));
    var checkCode =
        Sodium.cryptoBoxSeal(utf8.encode(jsonEncode(seed)), authorityPublicKey);
    return base64Url.encode(utf8.encode(String.fromCharCodes(checkCode)));
  }

  @override
  Widget build(BuildContext context) {
    return QrImage(
      data: _codeWrapperQR(),
      version: QrVersions.auto,
      foregroundColor:
          qrColor == null ? Theme.of(context).primaryColorLight : qrColor,
      padding: EdgeInsets.all(0),
    );
  }
}
