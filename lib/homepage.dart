import 'package:flutter/material.dart';
import 'package:naca/QRDeco.dart';
import 'package:naca/primaryButton.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/boutonScan.dart';
import 'package:naca/carouselQR.dart';
import 'package:naca/createEvent.dart';
import 'package:naca/models/Event.dart';
import 'package:naca/personalQR.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  Box<Event> hiveBox;

  @override
  void initState() {
    super.initState();
    hiveBox = Hive.box<Event>('event');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Empêche le layout de se réajuster lorsque le clavier apparaît
      resizeToAvoidBottomInset: false,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
              child: ValueListenableBuilder(
                valueListenable: hiveBox.listenable(),
                builder: (BuildContext context, Box eventBox, Widget child) {
                  Iterable<Event> events = eventBox.values;
                  List<Widget> pages = [];
                  pages.add(PersonalQR());
                  pages.addAll(events
                      .where((element) => element.created == true)
                      .map((e) {
                    return QRDeco(event: e);
                  }));
                  pages.add(CreateEvent());
                  return CarouselQR(pages);
                },
              ),
            ),
            Container(
              alignment: Alignment.bottomCenter,
              child: ButtonBar(
                buttonPadding: EdgeInsets.only(
                  left: 20,
                  right: 20,
                ),
                mainAxisSize: MainAxisSize
                    .min, // Prend le moins de place possible pour centrer
                children: <Widget>[
                  PrimaryButton(
                    body: 'Journal',
                    fontSize: 30.0,
                    onPressed: () => Navigator.pushNamed(context, '/journal'),
                  ),
                  BoutonScan(),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
