import 'dart:ui';

import 'package:flutter/material.dart';

/// Popup affichant un widget avec une animation de transition.
/// Cette classe n'est pas un widget, donc il est préférable de l'appeler comme une fonction lors d'un click.
/// L'appel ne marche pas si le layout est en cours de construction et ne marche pas non plus dans ``initState()``
///
/// Exemple d'usage :
/// ```
/// FloatingActionButton(
///   onPressed: () => Popup(
///     widget: Text('test'),
///     context: context,
///   ),
/// );
/// ```
class Popup {
  final Widget widget;
  Popup({BuildContext context, this.widget}) {
    // Fonction permettant d'afficher le popup
    showGeneralDialog(
      barrierColor: null,
      transitionBuilder: (context, a1, a2, widget) {
        final curvedValue = Curves.easeInOutBack.transform(a1.value) - 1.0;
        return Transform(
          // Matrice de translation
          transform: Matrix4.translationValues(
              0.0, -curvedValue * MediaQuery.of(context).size.height, 0.0),
          child: Opacity(
            // Changement d'opacité progressif
            // Petit problème: l'opacité de la couleur du container n'est pas rendue progressivement
            // TODO : décider si c'est utile ?
            opacity: a1.value,
            child: _PopupLayout(widget: this.widget),
          ),
        );
      },
      transitionDuration: Duration(milliseconds: 600),
      barrierDismissible: true,
      barrierLabel: '',
      context: context,
      pageBuilder: (context, animation1, animation2) {
        return;
      },
    );
  }
}

/// Layout du popup à faire apparaître
class _PopupLayout extends StatelessWidget {
  /// Widget à afficher dans le popup
  final Widget widget;

  const _PopupLayout({this.widget});

  @override
  Widget build(BuildContext context) {
    return Center(
      // Rend le popup scrollable quand il y a overflow avec le clavier
      child: SingleChildScrollView(
        child: Dialog(
          elevation: 0,
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.fromLTRB(10, 10, 10, 10),
          child: Stack(
            overflow: Overflow.visible,
            alignment: Alignment.center,
            children: <Widget>[
              ClipRect(
                // <-- clips to the 200x200 [Container] below
                child: BackdropFilter(
                  filter: ImageFilter.blur(
                    sigmaX: 20.0,
                    sigmaY: 20.0,
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width - 30,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(5.0),
                      color: const Color(0xEFEFE4).withOpacity(0.7),
                    ),
                    padding: EdgeInsets.all(20),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        widget,
                        Row(
                          children: [
                            Spacer(),
                            FlatButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              textColor: Theme.of(context).primaryColor,
                              child: const Text('Annuler'),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
