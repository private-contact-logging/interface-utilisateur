import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_barcode_scanner/flutter_barcode_scanner.dart';
import 'package:hive/hive.dart';
import 'package:toast/toast.dart';
import 'package:validators/validators.dart';
import 'models/Event.dart';

class BoutonScan extends StatefulWidget {
  const BoutonScan({Key key}) : super(key: key);

  @override
  BoutonScanState createState() => BoutonScanState();
}

class BoutonScanState extends State<BoutonScan> {
  Box<Event> event;

  @override
  void initState() {
    super.initState();
    event = Hive.box<Event>('event');
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 70,
      height: 70,
      child: FloatingActionButton(
        heroTag: null,
        key: Key('scan'),
        backgroundColor: Theme.of(context).primaryColor,
        mini: false,
        elevation: 0, //enlever l'ombre
        highlightElevation: 0,
        child: Container(
          padding: EdgeInsets.all(15),
          child: LayoutBuilder(builder: (context, constraint) {
            return Icon(Icons.qr_code_scanner_outlined,
                color: Theme.of(context).primaryColorLight,
                size: constraint.biggest.height);
          }),
        ),
        onPressed: () async => scanQR(),
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(50.0)),
      ),
    );
  }

  void showErr(){
    
  }

  Event parseQRContent(String qrContent) {
    Event addition;
    if (isURL(qrContent)) {
      // Ici on exécute le code pour décoder la classe protobuff obtenue d'un URL de crowdnotifier/notifyMe.
      // try {
        addition = Event.fromURL(qrContent);
      // } on FormatException catch (e) {
        //Toast.show(e.message, context);
      // } catch (e) {
        //Toast.show(e.message, context);
      // }
    } else if (isJSON(qrContent)) {
      // Ici on tente de décoder en JSON
      // try {
        addition = Event.fromJson(jsonDecode(qrContent));
      // } on PlatformException catch(e) {
        //Toast.show("Failed to get platform version > ${e.message}", context);
      // } on FormatException catch(e) {
        //Toast.show("Format Exception > ${e.message}", context);
      // }
    } else {
      //Toast.show("invalid QR Code", context);
    }

    return addition;
  }

  Future<void> scanQR() async {
    String res;
    res = await FlutterBarcodeScanner.scanBarcode(
        "#FFFFFF00", "Cancel", true, ScanMode.QR);
    final parsedEvent = parseQRContent(res);
    await event.add(parsedEvent);
    if (parsedEvent != null) {
      Toast.show("Added event is: " + parsedEvent.name, context);
    }
  }
}
