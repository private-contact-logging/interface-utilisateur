import 'dart:convert';
import 'dart:typed_data';
import 'package:hive/hive.dart';
import 'package:meta/meta.dart';
import 'package:naca/proto_out/qr.pbserver.dart';

part 'Event.g.dart';

@HiveType(typeId: 0)
class Event extends HiveObject {
  @HiveField(0)
  String name;
  @HiveField(1)
  String location;
  @HiveField(2)
  String context;
  @HiveField(3)
  DateTime startDate;
  @HiveField(4)
  DateTime endDate;
  @HiveField(5)
  //Evenement crée par l'application NACA -> True
  bool created;
  @HiveField(6)
  bool infected;
  @HiveField(7)
  List<int> _notifKey; // cle de tracage
  @HiveField(8)
  List<int> _secretKey; // cle secrete
  @HiveField(9)
  List<int> _salt;
  @HiveField(10)
  List<int> _publicKey;
  @HiveField(11)
  String trace;

  // Un événement créé par l'utilisateur.
  Event({
    // @required this.organisator,
    @required this.name,
    @required this.location,
    @required this.context,
    this.created: false,
    @required this.startDate, // valeur par defaut -> now
    this.endDate,
    this.infected,
    this.trace,
    // @required this.participant,
  }){
    _notifKey = [];
    _publicKey = [];
    _secretKey = [];
    _salt = [];
  }

  Uint8List get notifKey {
    return Uint8List.fromList(_notifKey);
  }

  Uint8List get publicKey {
    return Uint8List.fromList(_publicKey);
  }

  Uint8List get secretKey {
    return Uint8List.fromList(_secretKey);
  }

  Uint8List get salt {
    return Uint8List.fromList(_salt);
  }

  set notifKey(Uint8List params) {
    _notifKey = params.toList();
  }

  set secretKey(Uint8List params) {
    _secretKey = params.toList();
  }

  set salt(Uint8List params) {
    _salt = params.toList();
  }

  set publicKey(Uint8List params) {
    _publicKey = params.toList();
  }

  // Factory prennant un JSON en entrée pour créer un EventCreated
  factory Event.fromJson(Map<String, dynamic> json) {
    return new Event(
      name: json['content']['name'],
      location: json['content']['location'],
      context: json['content']['contexte'],
      startDate: DateTime.parse(json['content']['startDate']),
      endDate : json['content']['endDate'] == 'null' ? null : DateTime.parse(json['content']['endDate']),
      infected: false,
      trace: json['trace'],
    )
      .._notifKey = json['content']['notificationKey']?.cast<int>()
      .._publicKey = json['content']['publicKey']?.cast<int>();
  }

  factory Event.fromURL(String qrContent) {
    // Cette expression régulière a pour but de capturer tout ce qui vient après le premier "#"
    // Etant donné que crowdnotifier encode ses URLs de cette manière.
    RegExp urlMatcher =
        new RegExp(r"#[\w\W]+", caseSensitive: false, multiLine: false);
    String encodedObject = urlMatcher.stringMatch(qrContent);
    if (encodedObject == null) {
      throw FormatException(
          'The provided URL ' + qrContent + " does not match");
    }

    final normalizedBase64String =
        base64Url.normalize(encodedObject.substring(1));

    final byteList = base64Url.decode(normalizedBase64String);

    final protoBuf = QRCodeWrapper.fromBuffer(byteList);

    final protobufOutput = protoBuf.content;

    return Event(
      name: protobufOutput.hasName() ? protobufOutput.name : "No name",
      location: protobufOutput.hasLocation()
          ? protobufOutput.location
          : "No location",
      context: protobufOutput.hasVenueType()
          ? protobufOutput.venueType.toString()
          : "No context",
      startDate: protobufOutput.hasValidFrom()
          ? DateTime.fromMillisecondsSinceEpoch(
              protobufOutput.validFrom.toInt())
          : DateTime.now(),
      // TODO: Double check date format
      endDate: protobufOutput.hasValidTo()
          ? DateTime.fromMillisecondsSinceEpoch(protobufOutput.validTo.toInt())
          : null,
      created: false,
      infected: false,
    )
      .._notifKey = protobufOutput.hasNotificationKey()
          ? protobufOutput.notificationKey
          : null
      .._publicKey = protoBuf.hasPublicKey() ? protoBuf.publicKey : null;
      // On ne peut pas verifier l'infectiosité de l'événement si l'événement a été crée par CrNo.
  }
}
