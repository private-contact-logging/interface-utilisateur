///
//  Generated code. Do not modify.
//  source: qr.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import 'qr.pbenum.dart';

export 'qr.pbenum.dart';

class QRCodeWrapper extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QRCodeWrapper', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'org.crowdnotifier.android.sdk.model'), createEmptyInstance: create)
    ..a<$core.int>(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'version', $pb.PbFieldType.Q3)
    ..a<$core.List<$core.int>>(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'publicKey', $pb.PbFieldType.QY, protoName: 'publicKey')
    ..a<$core.List<$core.int>>(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'r1', $pb.PbFieldType.QY)
    ..aQM<QRCodeContent>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'content', subBuilder: QRCodeContent.create)
  ;

  QRCodeWrapper._() : super();
  factory QRCodeWrapper() => create();
  factory QRCodeWrapper.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QRCodeWrapper.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QRCodeWrapper clone() => QRCodeWrapper()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QRCodeWrapper copyWith(void Function(QRCodeWrapper) updates) => super.copyWith((message) => updates(message as QRCodeWrapper)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QRCodeWrapper create() => QRCodeWrapper._();
  QRCodeWrapper createEmptyInstance() => create();
  static $pb.PbList<QRCodeWrapper> createRepeated() => $pb.PbList<QRCodeWrapper>();
  @$core.pragma('dart2js:noInline')
  static QRCodeWrapper getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QRCodeWrapper>(create);
  static QRCodeWrapper _defaultInstance;

  @$pb.TagNumber(1)
  $core.int get version => $_getIZ(0);
  @$pb.TagNumber(1)
  set version($core.int v) { $_setSignedInt32(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasVersion() => $_has(0);
  @$pb.TagNumber(1)
  void clearVersion() => clearField(1);

  @$pb.TagNumber(2)
  $core.List<$core.int> get publicKey => $_getN(1);
  @$pb.TagNumber(2)
  set publicKey($core.List<$core.int> v) { $_setBytes(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPublicKey() => $_has(1);
  @$pb.TagNumber(2)
  void clearPublicKey() => clearField(2);

  @$pb.TagNumber(3)
  $core.List<$core.int> get r1 => $_getN(2);
  @$pb.TagNumber(3)
  set r1($core.List<$core.int> v) { $_setBytes(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasR1() => $_has(2);
  @$pb.TagNumber(3)
  void clearR1() => clearField(3);

  @$pb.TagNumber(4)
  QRCodeContent get content => $_getN(3);
  @$pb.TagNumber(4)
  set content(QRCodeContent v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasContent() => $_has(3);
  @$pb.TagNumber(4)
  void clearContent() => clearField(4);
  @$pb.TagNumber(4)
  QRCodeContent ensureContent() => $_ensure(3);
}

class QRCodeContent extends $pb.GeneratedMessage {
  static final $pb.BuilderInfo _i = $pb.BuilderInfo(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'QRCodeContent', package: const $pb.PackageName(const $core.bool.fromEnvironment('protobuf.omit_message_names') ? '' : 'org.crowdnotifier.android.sdk.model'), createEmptyInstance: create)
    ..aQS(1, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'name')
    ..aQS(2, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'location')
    ..aOS(3, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'room')
    ..e<QRCodeContent_VenueType>(4, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'venueType', $pb.PbFieldType.QE, protoName: 'venueType', defaultOrMaker: QRCodeContent_VenueType.OTHER, valueOf: QRCodeContent_VenueType.valueOf, enumValues: QRCodeContent_VenueType.values)
    ..a<$core.List<$core.int>>(5, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'notificationKey', $pb.PbFieldType.QY, protoName: 'notificationKey')
    ..a<$fixnum.Int64>(6, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validFrom', $pb.PbFieldType.OU6, protoName: 'validFrom', defaultOrMaker: $fixnum.Int64.ZERO)
    ..a<$fixnum.Int64>(7, const $core.bool.fromEnvironment('protobuf.omit_field_names') ? '' : 'validTo', $pb.PbFieldType.OU6, protoName: 'validTo', defaultOrMaker: $fixnum.Int64.ZERO)
  ;

  QRCodeContent._() : super();
  factory QRCodeContent() => create();
  factory QRCodeContent.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory QRCodeContent.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  QRCodeContent clone() => QRCodeContent()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  QRCodeContent copyWith(void Function(QRCodeContent) updates) => super.copyWith((message) => updates(message as QRCodeContent)); // ignore: deprecated_member_use
  $pb.BuilderInfo get info_ => _i;
  @$core.pragma('dart2js:noInline')
  static QRCodeContent create() => QRCodeContent._();
  QRCodeContent createEmptyInstance() => create();
  static $pb.PbList<QRCodeContent> createRepeated() => $pb.PbList<QRCodeContent>();
  @$core.pragma('dart2js:noInline')
  static QRCodeContent getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<QRCodeContent>(create);
  static QRCodeContent _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get name => $_getSZ(0);
  @$pb.TagNumber(1)
  set name($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasName() => $_has(0);
  @$pb.TagNumber(1)
  void clearName() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get location => $_getSZ(1);
  @$pb.TagNumber(2)
  set location($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasLocation() => $_has(1);
  @$pb.TagNumber(2)
  void clearLocation() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get room => $_getSZ(2);
  @$pb.TagNumber(3)
  set room($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasRoom() => $_has(2);
  @$pb.TagNumber(3)
  void clearRoom() => clearField(3);

  @$pb.TagNumber(4)
  QRCodeContent_VenueType get venueType => $_getN(3);
  @$pb.TagNumber(4)
  set venueType(QRCodeContent_VenueType v) { setField(4, v); }
  @$pb.TagNumber(4)
  $core.bool hasVenueType() => $_has(3);
  @$pb.TagNumber(4)
  void clearVenueType() => clearField(4);

  @$pb.TagNumber(5)
  $core.List<$core.int> get notificationKey => $_getN(4);
  @$pb.TagNumber(5)
  set notificationKey($core.List<$core.int> v) { $_setBytes(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasNotificationKey() => $_has(4);
  @$pb.TagNumber(5)
  void clearNotificationKey() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get validFrom => $_getI64(5);
  @$pb.TagNumber(6)
  set validFrom($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasValidFrom() => $_has(5);
  @$pb.TagNumber(6)
  void clearValidFrom() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get validTo => $_getI64(6);
  @$pb.TagNumber(7)
  set validTo($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasValidTo() => $_has(6);
  @$pb.TagNumber(7)
  void clearValidTo() => clearField(7);
}

