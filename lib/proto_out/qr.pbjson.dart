///
//  Generated code. Do not modify.
//  source: qr.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

const QRCodeWrapper$json = const {
  '1': 'QRCodeWrapper',
  '2': const [
    const {'1': 'version', '3': 1, '4': 2, '5': 5, '10': 'version'},
    const {'1': 'publicKey', '3': 2, '4': 2, '5': 12, '10': 'publicKey'},
    const {'1': 'r1', '3': 3, '4': 2, '5': 12, '10': 'r1'},
    const {'1': 'content', '3': 4, '4': 2, '5': 11, '6': '.org.crowdnotifier.android.sdk.model.QRCodeContent', '10': 'content'},
  ],
};

const QRCodeContent$json = const {
  '1': 'QRCodeContent',
  '2': const [
    const {'1': 'name', '3': 1, '4': 2, '5': 9, '10': 'name'},
    const {'1': 'location', '3': 2, '4': 2, '5': 9, '10': 'location'},
    const {'1': 'room', '3': 3, '4': 1, '5': 9, '10': 'room'},
    const {'1': 'venueType', '3': 4, '4': 2, '5': 14, '6': '.org.crowdnotifier.android.sdk.model.QRCodeContent.VenueType', '10': 'venueType'},
    const {'1': 'notificationKey', '3': 5, '4': 2, '5': 12, '10': 'notificationKey'},
    const {'1': 'validFrom', '3': 6, '4': 1, '5': 4, '10': 'validFrom'},
    const {'1': 'validTo', '3': 7, '4': 1, '5': 4, '10': 'validTo'},
  ],
  '4': const [QRCodeContent_VenueType$json],
};

const QRCodeContent_VenueType$json = const {
  '1': 'VenueType',
  '2': const [
    const {'1': 'OTHER', '2': 0},
    const {'1': 'MEETING_ROOM', '2': 1},
    const {'1': 'CAFETERIA', '2': 2},
    const {'1': 'PRIVATE_EVENT', '2': 3},
    const {'1': 'CANTEEN', '2': 4},
    const {'1': 'LIBRARY', '2': 5},
    const {'1': 'LECTURE_ROOM', '2': 6},
    const {'1': 'SHOP', '2': 7},
    const {'1': 'GYM', '2': 8},
    const {'1': 'KITCHEN_AREA', '2': 9},
    const {'1': 'OFFICE_SPACE', '2': 10},
  ],
};

