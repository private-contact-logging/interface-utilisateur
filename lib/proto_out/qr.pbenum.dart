///
//  Generated code. Do not modify.
//  source: qr.proto
//
// @dart = 2.3
// ignore_for_file: annotate_overrides,camel_case_types,unnecessary_const,non_constant_identifier_names,library_prefixes,unused_import,unused_shown_name,return_of_invalid_type,unnecessary_this,prefer_final_fields

// ignore_for_file: UNDEFINED_SHOWN_NAME
import 'dart:core' as $core;
import 'package:protobuf/protobuf.dart' as $pb;

class QRCodeContent_VenueType extends $pb.ProtobufEnum {
  static const QRCodeContent_VenueType OTHER = QRCodeContent_VenueType._(0, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OTHER');
  static const QRCodeContent_VenueType MEETING_ROOM = QRCodeContent_VenueType._(1, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'MEETING_ROOM');
  static const QRCodeContent_VenueType CAFETERIA = QRCodeContent_VenueType._(2, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CAFETERIA');
  static const QRCodeContent_VenueType PRIVATE_EVENT = QRCodeContent_VenueType._(3, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'PRIVATE_EVENT');
  static const QRCodeContent_VenueType CANTEEN = QRCodeContent_VenueType._(4, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'CANTEEN');
  static const QRCodeContent_VenueType LIBRARY = QRCodeContent_VenueType._(5, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LIBRARY');
  static const QRCodeContent_VenueType LECTURE_ROOM = QRCodeContent_VenueType._(6, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'LECTURE_ROOM');
  static const QRCodeContent_VenueType SHOP = QRCodeContent_VenueType._(7, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'SHOP');
  static const QRCodeContent_VenueType GYM = QRCodeContent_VenueType._(8, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'GYM');
  static const QRCodeContent_VenueType KITCHEN_AREA = QRCodeContent_VenueType._(9, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'KITCHEN_AREA');
  static const QRCodeContent_VenueType OFFICE_SPACE = QRCodeContent_VenueType._(10, const $core.bool.fromEnvironment('protobuf.omit_enum_names') ? '' : 'OFFICE_SPACE');

  static const $core.List<QRCodeContent_VenueType> values = <QRCodeContent_VenueType> [
    OTHER,
    MEETING_ROOM,
    CAFETERIA,
    PRIVATE_EVENT,
    CANTEEN,
    LIBRARY,
    LECTURE_ROOM,
    SHOP,
    GYM,
    KITCHEN_AREA,
    OFFICE_SPACE,
  ];

  static final $core.Map<$core.int, QRCodeContent_VenueType> _byValue = $pb.ProtobufEnum.initByValue(values);
  static QRCodeContent_VenueType valueOf($core.int value) => _byValue[value];

  const QRCodeContent_VenueType._($core.int v, $core.String n) : super(v, n);
}

