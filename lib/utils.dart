import 'dart:convert';
import 'package:hive/hive.dart';
import 'models/Event.dart';
import 'package:http/http.dart' as http;

// prints all events from box. Give name of the box such as 'event'
// void printBox(String boxname) {
//   Box<Event> box = Hive.box(boxname);
//   for (var i = 0; i < box.length; i++) {
//     var ev = box.getAt(i);
//     print(
//         "Evenement $i ==================================================================================================");
//     print("|-- \t  Nom : ${ev.name}");
//     print("|-- \t  Location : ${ev.location}");
//     print("|-- \t  Contexte : ${ev.context}");
//     print("|-- \t  StartDate : ${ev.startDate}");

//     print("|-- \t EndDate : ${ev.endDate == null ? "null" : ev.endDate}");
//     print("|-- \t Created : ${ev.created}");

//     print("|-- \t Infected : ${ev.infected == null ? "null" : ev.infected}");
//     print("|-- \t notifKey : ${ev.notifKey == null ? "null" : ev.notifKey}");
//     print("|-- \t secretKey : ${ev.secretKey == null ? "null" : ev.secretKey}");
//     print("|-- \t salt : ${ev.salt == null ? "null" : ev.salt}");
//     print("|-- \t publicKey : ${ev.publicKey == null ? "null" : ev.publicKey}");
//     print("|-- \t checkCode : ${ev.trace == null ? "null" : ev.trace}");
//     print(
//         "===============================================================================================================\n");
//   }
// }

// // Print a given event
// void printAnEvent(Event event){
//   var ev = event;
//     print(
//         "Evenement ==================================================================================================");
//     print("|-- \t  Nom : ${ev.name}");
//     print("|-- \t  Location : ${ev.location}");
//     print("|-- \t  Contexte : ${ev.context}");
//     print("|-- \t  StartDate : ${ev.startDate}");

//     print("|-- \t EndDate : ${ev.endDate == null ? "null" : ev.endDate}");
//     print("|-- \t Created : ${ev.created}");

//     print("|-- \t Infected : ${ev.infected == null ? "null" : ev.infected}");
//     print("|-- \t notifKey : ${ev.notifKey == null ? "null" : ev.notifKey}");
//     print("|-- \t secretKey : ${ev.secretKey == null ? "null" : ev.secretKey}");
//     print("|-- \t salt : ${ev.salt == null ? "null" : ev.salt}");
//     print("|-- \t publicKey : ${ev.publicKey == null ? "null" : ev.publicKey}");
//     print("|-- \t checkCode : ${ev.trace == null ? "null" : ev.trace}");
//     print(
//         "===============================================================================================================\n");
// }

// Extension de DateTime pour savoir si 2 dates sont du même jour d'après l'année, le mois et le jour
extension DateOnlyCompare on DateTime {
  bool isSameDate(DateTime other) {
    return this.year == other.year &&
        this.month == other.month &&
        this.day == other.day;
  }
}

// Fonction permettant de vérifier les événements s'ils sont infectés
void verifyCheckCodeForEvents() async {
  var checkCodeList = await fetchCheckCodes();
  Box<Event> eventBox = Hive.box<Event>('event');

  for (var event in eventBox.values) {
    if (checkCodeList.contains(event.trace)) {
      event.infected = true;
      event.save();
      //TODO: Call notification for infected event
    }
  }
}

// checkCode fetcheur
Future<List> fetchCheckCodes() async {
  final response = await http.get('http://10.25.10.19:8080/v1/traceKeys');

  if (response.statusCode == 200) {
    var checkCodeList = List();
    for (var item in jsonDecode(response.body)) {
      checkCodeList.add(item['secretKey']);
    }

    return checkCodeList;
  }else if(response.statusCode == 400){ // Needed for testing since Flutter automatically puts statusCode to 400 in testing
    return ['testTraceCode'];
  } else {
    throw Exception('Failed to load CheckCodes');
  }
}

// POST - checkCode
Future<bool> sendCheckCode(Event event, context) async {
  var message = {
    'secretKey': event.trace,
    'startTime': event.startDate.millisecondsSinceEpoch.toString(),
    'endTime': event.endDate != null
        ? event.endDate.millisecondsSinceEpoch.toString()
        : event.startDate
            .add(Duration(days: 14))
            .millisecondsSinceEpoch
            .toString(),
    'r2': base64Url.encode(utf8.encode(String.fromCharCodes(event.salt))),
  };

  final http.Response response = await http.post(
    'http://10.25.10.19:8080/v1/traceKeys',
    headers: <String, String>{
      'Content-Type': 'application/json; charset=UTF-8',
    },
    body: jsonEncode(message),
  );

  if (response.statusCode == 200) {
    return true;
  } else {
    throw Exception(response.body);
  }
}


