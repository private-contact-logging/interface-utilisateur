import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';

/// Carousel (Swiper) permettant de swiper entre différentes pages
class CarouselQR extends StatelessWidget {
  /// Liste de widgets à afficher, chacun correspondant à une page
  final List pages;
  CarouselQR(this.pages);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Swiper(
        itemBuilder: (BuildContext context, int index) {
          return pages[index];
        },
        // onTap: (x) => print('clicked $x'),
        loop: false,
        itemCount: pages.length,
        // Taille de la page en focus
        viewportFraction: 0.97,
        // Taille des pages qui ne sont pas en focus
        scale: 0.5,
        pagination: SwiperPagination(
          // Enlever la margin pour faire descendre la pagination
          margin: EdgeInsets.all(0.0),
          alignment: Alignment.bottomCenter,
          // Changement du style de la pagination
          builder: DotSwiperPaginationBuilder(
            color: Colors.grey,
            activeSize: 20,
            size: 20,
          ),
        ),
      ),
    );
  }
}
