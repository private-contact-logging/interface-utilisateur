// Define a custom Form widget.
import 'package:flutter/material.dart';
import 'package:naca/primaryButton.dart';

// TODO : réduire la taille des champs pour éviter de scroll pour le popup ?, tester les nouvelles options

/// Formulaire dynamique retournant une Map avec les inputs lors de la soumission
class DynamicForm extends StatefulWidget {
  /// Liste à 2 dimensions indiquant pour chaque champ son nom, son type, s'il est obligatoire et si besoin une valeur
  /// par défaut (placeholder) accompagnée d'un booléen indiquant sa visibilité. Ce dernier point décide seulement de si cette valeur par
  /// défaut doit être affichée dans le champ à remplir ou non. Le nom doit être distinct pour chaque champ.
  ///
  /// Cas particulier : si le champ est de type DateTime, n'est pas obligatoire et n'a pas de placeholder, alors
  /// le champ retournera null lors de la soumission du formulaire.
  ///
  /// Définition d'un champ :
  ///
  /// ``[Nom, Type, Obligatoire, Placeholder, Placeholder visible]`` (les deux derniers éléments sont optionnels)
  ///
  ///
  /// Par exemple:
  /// ```
  /// fields : [
  ///   ['Nom', String, true, 'John', true],
  ///   ['Date de naissance', DateTime, false],
  ///   ['Date initiale', DateTime, true, DateTime.now(), false],
  /// ]
  /// ```
  final List<dynamic> fields;

  /// Texte à afficher sur le bouton de validation
  final String buttonText;

  /// Référence vers la fonction à appeler lors de la soumission du formulaire.
  /// La Map avec les inputs sera passée en paramètre à cette fonction et les clés seront les noms des champs passés en paramètre
  ///
  /// Par exemple :
  ///
  /// ```
  /// maFonction(Map resultatFormulaire){
  ///   // Cela imprimerait ce que l'utilisateur a rempli dans le champ se nommant 'Nom'
  ///   print(resultatFormulaire['Nom']);
  /// }
  /// ```
  ///
  /// Ensuite il suffit d'utiliser la référence à cette fonction
  ///
  /// ``onSubmit: maFonction``
  final Function onSubmit;

  final BuildContext context;

  const DynamicForm({
    @required this.fields,
    @required this.buttonText,
    @required this.onSubmit,
    @required this.context,
  });

  @override
  DynamicFormState createState() {
    return DynamicFormState();
  }
}

class DynamicFormState extends State<DynamicForm> {
  // Crée une clé globale identifiant le formulaire
  // et permet de le valider
  final _formKey = GlobalKey<FormState>();
  var controllers = Map<String, Map<String, dynamic>>();
  var dates = List<DateTime>();
  bool validForm = true;
  bool fieldsReady = false;

  @override
  void dispose() {
    // On dispose de tous les controllers une fois que le Widget est détruit
    controllers.entries.forEach((element) {
      element.value.entries.forEach((el) {
        if (el.value is TextEditingController) el.value.dispose();
      });
    });
    super.dispose();
  }

  // Création des controllers seulement à l'initialisation
  // et pas dans le build comme il est appelé plusieurs fois
  @override
  void initState() {
    super.initState();
    TextEditingController controller;
    TextEditingController dateController;
    TextEditingController timeController;

    // Itération sur les champs
    widget.fields.forEach(
      (field) {
        if (field[1] == DateTime) {
          dateController = TextEditingController();
          timeController = TextEditingController();

          // S'il y a une valeur par défaut et qu'on souhaite qu'elle soit affichée
          if (field.length > 4 && field[4]) {
            dateController.text = dateFormater(field[3]);
            // On utilise le contexte du parent qui facilite l'appel,
            // sinon il faudrait faire un appel asynchrone qui ne smblait pas fonctionner dans les tests
            timeController.text =
                TimeOfDay.fromDateTime(field[3]).format(widget.context);
          }
          controllers[field[0]] = {
            // Des controllers individuels pour la date et l'heure
            'dateController': dateController,
            'timeController': timeController,
            // Stockage en parallèle de la vraie valeur pour éviter de devoir reconvertir
            // vu que le controller peut seulement stocker du texte en String, permet aussi de ne pas afficher
            // la valeur par défaut et assignation de la valeur par défaut si défini
            'dateValue': field.length > 3 ? field[3] : null,
            'timeValue': field.length > 3
                ? (field[3] == null ? null : TimeOfDay.fromDateTime(field[3]))
                : null,
            'type': DateTime,
            'required': field[2],
          };
        } else if (field[1] == String) {
          controller = TextEditingController();
          // Assignation de la valeur par défaut si défini
          if (field.length > 4 && field[4]) controller.text = field[3];
          controllers[field[0]] = {
            'fieldController': controller,
            'type': String,
            'required': field[2],
          };
          if (field.length > 4 && !field[4])
            controllers[field[0]]['value'] = field[3];
        }
        // La gestion pour d'autres types peut être ajoutée
      },
    );
  }

  /// Méthode construisant tous les champs nécessaires. Le paramètre est nécessaire pour inciter la méthode
  /// build à reconstruire les éléments du tree, sinon ``setState`` ne fonctionne pas
  List<Widget> buildFields(bool dummy) {
    // Variable possédant tous les éléments du formulaire à afficher
    final processedFields = <Widget>[];
    var placeholder;
    var textInputAction;

    // Construction et ajout des champs selon les paramètres entrés
    for (int i = 0; i < controllers.entries.length; i++) {
      var field = controllers.entries.elementAt(i);

      // Ajout de l'astérisque si le champ est obligatoire et d'un message d'erreur
      if (field.value['required']) {
        placeholder = field.key + '*';
      } else {
        placeholder = field.key;
      }

      // Champ de type String
      if (field.value['type'] == String) {
        // Assignation de textInputAction qui détermine si l'utilisateur peut accéder au champ suivant
        // directement depuis le clavier. C'est désactivé (done) si c'est le dernier champ ou si
        // le prochain champ est de type DateTime et requiert que l'utilisateur tape sur le champ pour faire
        // apparaître le calendrier ou l'horloge
        if (i == controllers.entries.length - 1 ||
            controllers.entries.elementAt(i + 1).value['type'] == DateTime)
          textInputAction = TextInputAction.done;
        else
          textInputAction = TextInputAction.next;

        processedFields.add(
          _buildStringField(placeholder, field.value['required'], context,
              controllers[field.key]['fieldController'], textInputAction),
        );
        // Champ de type DateTime
      } else if (field.value['type'] == DateTime) {
        // Affichage avec le titre du champ au-dessus des champs pour remplir la date et l'heure
        processedFields.add(
          Container(
            margin: EdgeInsets.only(top: 40, bottom: 10),
            child: Text(
              placeholder,
              style: TextStyle(color: Theme.of(context).primaryColor),
            ),
          ),
        );
        processedFields.add(
          // Le LayoutBuilder permet de récupérer la taille du parent
          LayoutBuilder(
            builder: (BuildContext context, BoxConstraints constraints) {
              return Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    // On récupère la largeur
                    width: constraints.maxWidth * 0.55,
                    child: _buildDateField(
                      'Date',
                      field.value['required'],
                      context,
                      controllers[field.key]['dateController'],
                      controllers[field.key],
                    ),
                  ),
                  Container(
                    width: constraints.maxWidth * 0.4,
                    child: _buildTimeField(
                      'Heure',
                      field.value['required'],
                      context,
                      controllers[field.key]['timeController'],
                      controllers[field.key],
                    ),
                  ),
                ],
              );
            },
          ),
        );
      }
    }
    // Ajout de l'explication de l'astérisque
    processedFields.add(
      Container(
        margin: EdgeInsets.only(top: 50, bottom: 10),
        // Animation de transition agrandissant le texte et changeant la couleur en rouge, si le
        // formulaire n'est pas valide lors de la soumission
        child: AnimatedDefaultTextStyle(
          child: Text('*Champs obligatoires'),
          style: validForm
              ? TextStyle(color: Theme.of(context).primaryColor, fontSize: 10)
              : TextStyle(
                  color: Colors.red,
                  fontSize: 15,
                ),
          duration: Duration(milliseconds: 1500),
          curve: Curves.elasticOut,
        ),
      ),
    );

    // Ajout du bouton de validation
    processedFields.add(
      Padding(
        padding: const EdgeInsets.only(top: 40.0),
        child: PrimaryButton(
          body: widget.buttonText,
          fontSize: 30.0,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          onPressed: validateForm,
        ),
      ),
    );

    return processedFields;
  }

  /// Méthode validant le formulaire
  void validateForm() {
    // Test de validité du formulaire
    if (_formKey.currentState.validate()) {
      // Dictionnaire en retour avec les bons types
      var resMap = Map();
      controllers.entries.forEach(
        (controller) {
          if (controller.value['type'] == String) {
            controller.value.containsKey('value') &&
                    controller.value['fieldController'].text.length == 0
                ? resMap[controller.key] = controller.value['value']
                : resMap[controller.key] =
                    controller.value['fieldController'].text;
          } else if (controller.value['type'] == DateTime) {
            // Combinaison des champs date et heure si la date n'est pas null
            var selectedDate = controller.value['dateValue'];
            var selectedTime = controller.value['timeValue'];
            // Test de tout type d'entrée possible, qui peuvent arriver si le champ est optionnel
            if (selectedDate == null) {
              resMap[controller.key] = null;
            } else if (selectedDate != null && selectedTime == null) {
              resMap[controller.key] = DateTime(
                selectedDate.year,
                selectedDate.month,
                selectedDate.day,
              );
            } else {
              resMap[controller.key] = DateTime(
                selectedDate.year,
                selectedDate.month,
                selectedDate.day,
                selectedTime.hour,
                selectedTime.minute,
              );
            }
          }
        },
      );
      // Appel de la fonction passée en paramètre au widget
      widget.onSubmit(resMap);
    } else {
      setState(() {
        validForm = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    // Construction du formulaire avec la clé créée
    return Form(
      key: _formKey,
      child: SingleChildScrollView(
        child: Column(
          children: buildFields(validForm),
        ),
      ),
    );
  }
}

/// Méthode définissant le style par défaut d'un champ
InputDecoration _buildInputDecoration(
  String hint,
  BuildContext context,
) {
  final border = OutlineInputBorder(
    borderRadius: BorderRadius.circular(25.0),
    borderSide: BorderSide(
      color: Theme.of(context).primaryColorLight,
    ),
  );
  final errorBorder = OutlineInputBorder(
    borderRadius: BorderRadius.circular(25.0),
    borderSide: BorderSide(
      color: Colors.red,
    ),
  );
  return InputDecoration(
    fillColor: Theme.of(context).primaryColorLight,
    filled: true,
    enabledBorder: border,
    focusedBorder: border,
    contentPadding: EdgeInsets.all(20),
    labelText: hint,
    errorStyle: TextStyle(
      height: 0,
    ),
    errorBorder: errorBorder,
    focusedErrorBorder: errorBorder,
  );
}

/// Méthode créant un champ permettant d'entrer un input de type String
Widget _buildStringField(
  String name,
  bool requiredField,
  BuildContext context,
  TextEditingController controller,
  TextInputAction textInputAction,
) {
  return Container(
    margin: EdgeInsets.only(top: 20),
    child: TextFormField(
      cursorColor: Theme.of(context).primaryColor,
      validator: customValidator(requiredField),
      decoration: _buildInputDecoration(name, context),
      controller: controller,
      textInputAction: textInputAction,
      style: TextStyle(color: Theme.of(context).primaryColor),
    ),
  );
}

/// Méthode créant un champ permettant d'entrer un input de type DateTime
Widget _buildDateField(
  String name,
  bool requiredField,
  BuildContext context,
  TextEditingController controller,
  Map value,
) {
  return TextFormField(
    // Removes cursor and pointer
    showCursor: false,
    enableInteractiveSelection: false,
    onTap: () {
      // Ligne pour empêcher le clavier d'apparaître
      FocusScope.of(context).requestFocus(new FocusNode());
      // Appelle de la méthode affichant le calendrier
      _selectDate(context, controller, value);
    },
    validator: customValidator(requiredField),
    decoration: _buildInputDecoration(name, context),
    controller: controller,
    style: TextStyle(color: Theme.of(context).primaryColor),
  );
}

/// Méthode créant un champ permettant d'entrer un input de type DateTime
Widget _buildTimeField(
  String name,
  bool requiredField,
  BuildContext context,
  TextEditingController controller,
  Map value,
) {
  return TextFormField(
    // Enlève le curseur et le pointeur
    showCursor: false,
    enableInteractiveSelection: false,
    onTap: () {
      // Ligne pour empêcher le clavier d'apparaître
      FocusScope.of(context).requestFocus(new FocusNode());
      // Appelle de la méthode affichant l'horloge
      _selectTime(context, controller, value);
    },
    validator: customValidator(requiredField),
    decoration: _buildInputDecoration(name, context),
    controller: controller,
    style: TextStyle(color: Theme.of(context).primaryColor),
  );
}

/// Méthode affichant un calendrier permettant de sélectionner une date
Future<Null> _selectDate(
  BuildContext context,
  TextEditingController controller,
  Map value,
) async {
  final DateTime picked = await showDatePicker(
    context: context,
    initialDate: DateTime.now(),
    firstDate: DateTime.now(),
    lastDate: DateTime.now().add(Duration(days: 365)),
    // Ajout du colorScheme pour le thème du calendrier
    builder: (BuildContext context, Widget child) {
      return Theme(
        data: Theme.of(context).copyWith(
          colorScheme: ColorScheme.light().copyWith(
            primary: Theme.of(context).primaryColor,
          ),
        ),
        child: child,
      );
    },
  );
  if (picked != null) {
    // Conversion de la date en string
    controller.text = dateFormater(picked);
    value['dateValue'] = picked;
  }
}

/// Méthode affichant une horloge permettant de sélectionner une heure
Future<Null> _selectTime(
  BuildContext context,
  TextEditingController controller,
  Map value,
) async {
  final TimeOfDay picked = await showTimePicker(
    context: context,
    initialTime: TimeOfDay.now(),
    builder: (BuildContext context, Widget child) {
      return Theme(
        data: Theme.of(context).copyWith(
          colorScheme: ColorScheme.light().copyWith(
            primary: Theme.of(context).primaryColor,
          ),
        ),
        child: child,
      );
    },
  );
  if (picked != null) {
    controller.text = picked.format(context);
    value['timeValue'] = picked;
    // Assignation de la date si ce n'est pas fait
    if (value['dateController'].text.length == 0) {
      if (value['dateValue'] == null) {
        value['dateValue'] = DateTime.now();
      }
      value['dateController'].text = dateFormater(value['dateValue']);
    }
  }
}

/// Méthode formattant la date en omettant l'heure. Retourne par exemple ``20/12/21``
String dateFormater(DateTime date) {
  return date.day.toString() +
      '/' +
      date.month.toString() +
      '/' +
      date.year.toString();
}

Function customValidator(bool requiredField) {
  return requiredField ? (value) => value.isEmpty ? '' : null : null;
}
