import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/models/Event.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:naca/QR.dart';
import 'package:qr_flutter/qr_flutter.dart';

void main() {
  Box<Event> eventDB;

  setUpAll(() async {
    // We init the Hive
    await Hive.initFlutter();
    // Since our classes are not native elements, we generate Adapters
    Hive.registerAdapter<Event>(EventAdapter());
    // We open the boxes, we can open them anywhere.
    // It is better to close them after using it.

    eventDB = await Hive.openBox<Event>('event');
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  testWidgets('Affichage QR', (WidgetTester tester) async {
    Event event5 = new Event(
        name: 'Rendez-vous',
        location: 'la-bas',
        context: 'RDV',
        startDate: DateTime(2021, 16, 02));

    eventDB.add(event5);

    QR testQR = QR.fromEvent(event5);

    await tester.pumpWidget(makeTesteableWidget(child: testQR));

    final qrFinder = find.byType(QrImage);
    expect(qrFinder, findsOneWidget);
  });
}

Widget makeTesteableWidget({Widget child}) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}
