import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:naca/models/Event.dart';
import "package:naca/models/User.dart";

void main() async {
  // We init the Hive
  await Hive.initFlutter();
  // Since our classes are not native elements, we generate Adapters
  Hive.registerAdapter<Event>(EventAdapter());
  Hive.registerAdapter<User>(UserAdapter());
  // We open the boxes, we can open them anywhere.
  // It is better to close them after using it.

  // Event box
  Box<Event> event;
  Map<String, dynamic> jsonQR;
  String url;
  group('Test on Event Box from URL and JSON', () {
    setUp(() async {
      event = await Hive.openBox<Event>('event');
      await event.clear();
      jsonQR = {
        'signature': null,
        'content': {
          'name': 'Anniversaire Xoeseko',
          'location': 'Grand Rue',
          'contexte': 'anniversaire',
          'startDate': DateTime(2020, 14, 12).toString(),
          'endDate': DateTime(2020, 15, 12).toString(),
          'notifKey': null,
          'publicKey': null,
        },
        'trace': 'traceCode',
      };

      url =
          "https://qr.notify-me.ch/?v=1#CAESILtbhP17ywgnvsSANufbF1k4ryuvD4OqFX3JJFAnMAALGiDViexQ2ySg0o9pPg25AFdw_Sk3Wb9SIWZf6jDS5Sx5iiJSCgxBbm5pdmVyc2FpcmUSC0NvdmlkLXBhcnR5GgNHc3ggAyoglh9sgv8sNs0hJx9unv3JU39WPi2c9OYnxBR5tqoD9jEwgJPU8uUuOIDL7ZvmLg";
    });

    tearDown(() async {
      await event.close();
    });
    // Testing CRUD on EventCreated DB
    // Create & Read
    test('Create Event from JSON and URL', () {
      var eventFromJSON1 = Event.fromJson(jsonQR);
      var eventFromURL = Event.fromURL(url);
      var eventFromJSON2 = Event.fromJson(jsonQR);

      //Create
      event.put('eventFromJson1', eventFromJSON1);
      event.put('eventFromJson2', eventFromJSON2);
      event.put('eventFromURL', eventFromURL);
      //Read
      var resURL = event.get('eventFromURL');

      //Update
      var resEventFromJSON1 = event.get('eventFromJson1');
      resEventFromJSON1.location = 'Grand Rue 75, Morges';
      resEventFromJSON1.save();
      //Delete
      eventFromJSON2.delete();

      // Only json1 and url should be in the box.
      expect(event.keys.contains('eventFromJson1'), true); // checking the key for JSON1
      expect(event.keys.contains('eventFromURL'), true); // checking the key for URL
      expect(eventFromURL.location, 'Covid-party'); //checks field of URL
      expect(resEventFromJSON1.trace,'traceCode'); // checks if traceCode was properly parsed
      expect(resURL.name, 'Anniversaire'); // checks Read
      expect(event.length, 2); // checks & delete
      expect(eventFromJSON1.location, 'Grand Rue 75, Morges'); //checks update
    });
  });

  group('Test errors thrown by constructor', () {
    test('A random character string throws format exception', () {
      expect(() => Event.fromURL('blatently false url'), throwsException);
    });
    test('A wrong url throws a format exception', () {
      expect(() => Event.fromURL('example.com/balblabla'), throwsException);
    });
  });
}
