// Import the test package and Counter class
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:naca/dynamicForm.dart';
import 'package:naca/primaryButton.dart';

void main() {
  var res;
  final mandatoryForm = makeTesteableWidget(
      child: Builder(
    builder: (BuildContext context) => DynamicForm(
      fields: [
        ['Nom', String, true],
        ['Lieu', String, true],
        ['Contexte', String, false],
        ['Date de début', DateTime, true, DateTime.now(), false],
        ['Date de fin', DateTime, false, null, false],
      ],
      buttonText: 'Créer',
      onSubmit: (formRes) => res = formRes,
      context: context,
    ),
  ));

  testWidgets('DynamicForm creates the TextFormFields with mandatory fields',
      (WidgetTester tester) async {
    // Create the widget by telling the tester to build it.

    await tester.pumpWidget(mandatoryForm);

    // Test du nombre de champs générés
    // 3 champs String et 2 champs DateTime comprenant chacun 2 champs (pour la date et pour l'heure)
    // 3 + 2 * 2 = 7
    expect(find.byType(TextFormField), findsNWidgets(7));

    // Test de la création des champs avec les bon noms, avec astérisque si obligatoire
    expect(find.text('Nom*'), findsOneWidget);
    expect(find.text('Lieu*'), findsOneWidget);
    expect(find.text('Contexte'), findsOneWidget);
    expect(find.text('Date de début*'), findsOneWidget);
    expect(find.text('Date de fin'), findsOneWidget);

    expect(find.text('Créer'), findsOneWidget);

    await tester.tap(find.byType(PrimaryButton));
    await tester.pumpAndSettle();

    // On s'attend à ne rien recevoir en retour, comme les champs obligatoires sont vides
    expect(res, isNull);

    // TODO trouver comment tester l'activation des messages d'erreur lorsque l'on valide le formulaire vide
    /*expect(
        ((tester.firstWidget(find.text('*Champs obligatoires')) as Text).style)
            .color,
        Colors.red);*/
  });

  //TODO appuyer sur le bouton de validation ne semble pas marcher
}

Widget makeTesteableWidget({Widget child}) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}
