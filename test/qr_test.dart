import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/models/Event.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:naca/QR.dart';

void main() async {
  //init de la box HIVE
  await Hive.initFlutter();
  Hive.registerAdapter<Event>(EventAdapter());
  Box<Event> eventDB;

  setUp(() async {
    eventDB = await Hive.openBox<Event>('event');
    await eventDB.clear();
  });

  test('Une clé publique est generée', () {
    Event event = new Event(
        name: 'Présentation',
        location: 'CUI',
        context: 'Cours',
        startDate: DateTime.now());

    eventDB.add(event);

    QR testQR = QR.fromEvent(event);

    expect(testQR.publicKey, isNot([]));
  });

  test('Deux event genère des clés différentes', () {
    Event event1 = new Event(
        name: 'Présentation',
        location: 'CUI',
        context: 'Cours',
        startDate: DateTime.now());

    eventDB.add(event1);
    QR testQR1 = QR.fromEvent(event1);

    Event event2 = new Event(
        name: 'Fete de fin dannée',
        location: 'Chez moi',
        context: 'Fête',
        startDate: DateTime(2020, 31, 12));

    eventDB.add(event2);
    QR testQR2 = QR.fromEvent(event2);

    expect(testQR1.publicKey, isNot(testQR2.publicKey));
    expect(testQR1.secretKey, isNot(testQR2.secretKey));
  });
}
