// Import the test package and Counter class
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:naca/popup.dart';

void main() {
  testWidgets('The Popup appears, displays the correct widget and disappears',
      (WidgetTester tester) async {
    await tester.pumpWidget(makeTesteableWidget(child: PopupTestWidget()));

    // Clique sur le bouton pour faire apparaître le popup
    await tester.tap(find.byType(FloatingActionButton));
    await tester.pump();

    expect(find.text('test'), findsOneWidget);

    // Clique sur le bouton pour faire disparaître le popup
    await tester.tap(find.byType(FloatingActionButton));
    await tester.pump();

    expect(find.text('test'), findsNothing);
  });
}

// Widget avec un bouton pour faire apparaître le Popup
class PopupTestWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return FloatingActionButton(
      onPressed: () => Popup(
        widget: Text('test'),
        context: context,
      ),
    );
  }
}

Widget makeTesteableWidget({Widget child}) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}
