// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

import 'package:naca/main.dart';
import 'package:naca/models/Event.dart';
import 'package:naca/models/User.dart';

void main() {
  group('Les wdigets sont présentes', () {
    setUpAll(() async {
      // TODO: Abstraire dans le setup dans une classe utils de test.
      // We init the Hive
      await Hive.initFlutter();
      // Since our classes are not native elements, we generate Adapters
      Hive.registerAdapter<Event>(EventAdapter());
      Hive.registerAdapter<User>(UserAdapter());
      // We open the boxes, we can open them anywhere.
      // It is better to close them after using it.
      // TODO: Check bug where specifying type <EventParticipated> causes already initilalized error.
      await Hive.openBox<Event>('event');
      await Hive.openBox<User>('user');
      TestWidgetsFlutterBinding.ensureInitialized();
    });

    testWidgets('Le texte "Mon QR" est présent au lancement',
        (WidgetTester tester) async {
      // Build our app and trigger a frame.
      await tester.pumpWidget(CovidApp());

      // Verify that the text is present on start screen.
      expect(find.text('Mon QR'), findsOneWidget);
    });

    testWidgets('Le bouton scan est présent', (WidgetTester tester) async {
      await tester.pumpWidget(CovidApp());

      // Verifiy that the button is present on start screen.
      expect(find.byIcon(Icons.qr_code_scanner_outlined), findsOneWidget);
    });
  });
}
