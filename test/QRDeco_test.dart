import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:naca/QRDeco.dart';

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/models/Event.dart';
import 'package:naca/primaryButton.dart';
import 'package:naca/secondaryButton.dart';
import 'package:qr_flutter/qr_flutter.dart';

void main() {
  Box<Event> eventDB;

  setUpAll(() async {
    // We init the Hive
    await Hive.initFlutter();
    // Since our classes are not native elements, we generate Adapters
    Hive.registerAdapter<Event>(EventAdapter());
    // We open the boxes, we can open them anywhere.
    // It is better to close them after using it.

    eventDB = await Hive.openBox<Event>('event');
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  testWidgets('QR, text and all the screens to signal an event are displayed',
      (WidgetTester tester) async {
    Event event = new Event(
      name: 'Rendez-vous',
      location: 'la-bas',
      context: 'RDV',
      startDate: DateTime(2021, 16, 02),
      infected: false,
    );

    eventDB.add(event);

    final qrDeco = makeTesteableWidget(
      child: QRDeco(event: event),
    );

    await tester.pumpWidget(qrDeco);

    // Affichage initial: QR et nom d'événement
    expect(find.byType(QrImage), findsOneWidget);

    expect(find.text(event.name), findsOneWidget);

    // On veut le design avec le virus au début pour le QR

    expect(find.byKey(Key('virusQR')), findsOneWidget);

    // On appuie sur le QR pour afficher le design alternatif

    await tester.tap(find.byType(QrImage));

    await tester.pumpAndSettle();

    expect(find.byKey(Key('altQR')), findsOneWidget);

    // On reclick pour afficher le design original

    await tester.tap(find.byType(QrImage));

    await tester.pumpAndSettle();

    expect(find.byKey(Key('virusQR')), findsOneWidget);

    // On appuie sur le bouton pour afficher le bouton de signalement
    await tester.tap(find.byType(FlatButton));

    // On a besoin du settle vu qu'il y a une transition entre les états
    await tester.pumpAndSettle();

    expect(find.text("Signaler\ncet événement"), findsOneWidget);

    expect(find.byType(PrimaryButton), findsOneWidget);

    // On navigue jusqu'à l'écran de confirmation

    await tester.tap(find.byType(PrimaryButton));

    await tester.pump();

    expect(find.byType(PrimaryButton), findsOneWidget);

    expect(
        find.text(
            "Souhaitez-vous vraiment signaler un cas de covid-19 à cet événement ?"),
        findsOneWidget);

    expect(find.byType(SecondaryButton), findsNWidgets(2));

    expect(find.text("Cette action est irréversible"), findsOneWidget);

    // Retour à l'écran initial

    await tester.tap(find.text("Non"));

    await tester.pumpAndSettle();

    expect(find.byType(QrImage), findsOneWidget);

    expect(find.text(event.name), findsOneWidget);

    // TODO vérifier le click sur "Oui" et attendre la réponse du backend avant de tester
  });
}

Widget makeTesteableWidget({Widget child}) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}
