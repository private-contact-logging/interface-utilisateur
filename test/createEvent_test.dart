import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/createEvent.dart';
import 'package:naca/dynamicForm.dart';
import 'package:naca/models/Event.dart';

void main() {
  setUpAll(() async {
    // We init the Hive
    await Hive.initFlutter();
    // Since our classes are not native elements, we generate Adapters
    Hive.registerAdapter<Event>(EventAdapter());
    // We open the boxes, we can open them anywhere.
    // It is better to close them after using it.

    await Hive.openBox<Event>('event');
    TestWidgetsFlutterBinding.ensureInitialized();
  });

  testWidgets('Create event screen is displayed and button works',
      (WidgetTester tester) async {
    await tester.pumpWidget(makeTesteableWidget(child: CreateEvent()));

    // Test des éléments de départ
    expect(find.text("Créer un événement"), findsOneWidget);
    expect(find.byType(FloatingActionButton), findsOneWidget);

    // Test d'apparition du popup
    await tester.tap(find.byType(FloatingActionButton));
    await tester.pumpAndSettle();

    expect(find.byType(DynamicForm), findsOneWidget);
  });
}

Widget makeTesteableWidget({Widget child}) {
  return MaterialApp(
    home: Scaffold(
      body: child,
    ),
  );
}
