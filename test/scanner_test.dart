import 'package:flutter_test/flutter_test.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:naca/boutonScan.dart';
import 'package:naca/models/Event.dart';
void main() async {
  await Hive.initFlutter();
  Hive.registerAdapter<Event>(EventAdapter());
  BoutonScanState testButton;
  setUp(() async {
    testButton = new BoutonScanState();
    await Hive.openBox<Event>('event');
    testButton.initState();
  });

  group('Parsing', () {
    // test('notifyMe json string returns', () {
    //   expect(() => testButton.parseQRContent(), returnsNormally);
    // });

    test('our json string returns', () {
      expect(() => testButton.parseQRContent('''{
	"signature": [7, 136, 232, 147, 233, 31, 57, 223, 214, 162, 68, 71, 229, 136, 123, 156, 59, 81, 173, 252, 142, 177, 167, 66, 112, 123, 240, 190, 202, 61, 4, 147, 215, 141, 118, 209, 180, 119, 74, 25, 7, 16, 110, 107, 33, 140, 123, 142, 173, 223, 226, 145, 84, 226, 82, 231, 188, 45, 92, 244, 145, 28, 115, 3],
	"content": {
		"name": "ParsingTestEvent",
		"location": "Home",
		"contexte": "Testing",
		"startDate": "2021-01-24 13:17:00.000",
		"endDate": "2021-01-24 17:17:00.000",
		"notifKey": [201, 105, 132, 150, 212, 167, 127, 2, 83, 96, 35, 234, 80, 51, 195, 36, 224, 89, 66, 49, 230, 5, 89, 216, 72, 248, 159, 95, 131, 117, 165, 232],
		"publicKey": [149, 155, 197, 20, 150, 236, 143, 128, 57, 245, 130, 57, 9, 23, 157, 164, 211, 77, 112, 30, 13, 152, 196, 253, 252, 68, 84, 239, 20, 117, 99, 11]
	},
	"trace": "IMO_LcKNw7LDmHPDp8OQw6Qdw6RtAMOTZ0RaGMK1MyHDrsObwo1JLMKZf8O4wr8NSsOjLMKiDsKIw5nDhcK5EMOqw5E0BmHDj8OMw43Dj8ONUhdPasOxwoHCjBHDucOOwqTCmcKTwo7ChB_Cv8KYZCRwCWNpw4jCu1vCkXHDjF8swqtlU8OtNzHCvSxiw7DCgz7DmCUNC8O3LzLDkcK2YMKrwrwIwpPDosKwHsK1PMK8w5dmczXDpgTCisKLw7Ubw7rDknl9woQvXcKzVsOlLsKiw6M2BHHDgEHCjC1gw5lHw6fDi08_asO1IBV6w5IGwrLCosO4VMOEwrPCrMOWRRPCrMK7FHxgw4ZrPHAvPMKGfMK7w4sYQcOQe03DqTjCgRtiLy_Di0jDpsKJAsOzw63CtsOxwqpLJsK8YcO6woTDgBbDtXfDocKTwpXDhMO6aWo2wqrCj2LCnF7CkcK4TMOuwr7CrMK0SMK5w69qc3PCli4JaEURAlrDtsOrG2QswogvwrbDsT5uw4w_CMObFXnDhl_ChcO_csKxScKlwpzCkcKAwoDDp2bDicOdK30lKcK1Cg0NK8O0wqzDsH5gCMORwpwDw7gpHA_DpGTDkSQuw7zDnn7DqBRIZ2pXwojCocKuwrcxGQ7Ct8KyFH_CkBcFIsOtOzYcGhDDmyg="
}'''), returnsNormally);
    });
  });
  // Le test suivant n'ext pas executé car ce n'est pas possible de faker une méthode statique.
  // C'est tout de même intéressant de laisser la tentative à des fins de documentation.
  group('Scanner', () {
    test('returns if the scan succeeds', () {
      // when(MockScanner.scanBarcode()).thenAnswer((_) =>
      //     '{ "version": 1, "content": { "version": 1, "publicKey": "LdMcqMPUgixrY9F4ItaGd5ZIbdB39TvH64UX98f5HSw=", "name": "Test event", "location": "Test Location", "room": "2", "venueType": "PRIVATE_PARTY", "notificationKey": "KYBjN9s+ZxmBRr3BC4eA5kZqjQsfDLzSqtoLdWzo2P8=" }, "signature": "Ig71CPIbm4am1qN/zA7kqg8LFZQj3L7D12LMNl2uT0eqIZIA85sgc+M/EEH2SdKAGd9wLxV0FYU7U0VnUzFgBA==" }');
    });
  }, skip: true);

  test('throws format exception for wrong format', () {
    expect(() => testButton.parseQRContent('lkjsd'), throwsException);
    // expect(
    //     () => testButton.parseQRContent(
    //         '{ "version": 1, "content": { "version: 1, "pubUgixrY9F4ItaGd5ZIbdB39TvH64UX98f5HSw=", "name": "Test event", "location": "Test Location", "room": "2", "venu"PRIVATE_PARTY", "notificationKey": "KYBjN9s+ZxmBRr3BC4eA5kZqjQsfDLzSqtoLdWzo2P8=" }, "signature": "Ig71CPIbm4am1qN/KAGd9wLxV0FYU7U0VnUzFgBA== }'),
    //     throwsException);
  }, skip: true);
}

// class MockScanner extends Mock implements FlutterBarcodeScanner {}
