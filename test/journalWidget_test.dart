import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:intl/intl.dart';
import 'package:naca/journal.dart';
import 'package:naca/main.dart';
import 'package:naca/models/Event.dart';
import 'package:naca/models/User.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:intl/date_symbol_data_local.dart';

void main() {
  Box<Event> eventDB;
  ThemeData theme = new ThemeData(
    primaryColor: const Color(0xFF05668D), //bleu
    accentColor: const Color(0xFF00A896), //vert
    primaryColorLight: const Color(0xFFF0F3BD), //jaune
    secondaryHeaderColor: const Color(0x9900A896), // vert moins opaque
    fontFamily: 'Gabriela',
  );
  group('Test du journal', () {
    setUpAll(() async {
      // We init the Hive
      await Hive.initFlutter();
      // Since our classes are not native elements, we generate Adapters
      Hive.registerAdapter<Event>(EventAdapter());
      Hive.registerAdapter<User>(UserAdapter());
      // We open the boxes, we can open them anywhere.
      // It is better to close them after using it.
      eventDB = await Hive.openBox<Event>('event');
      await Hive.openBox<User>('user');
      TestWidgetsFlutterBinding.ensureInitialized();
      await initUser();
      await initializeDateFormatting('fr');
      eventDB.clear();
    });

    tearDown(() async {
      eventDB.clear();
    });

    testWidgets('The journal page and all its components are displayed',
        (WidgetTester tester) async {
      Event mockEvent = new Event(
        name: 'JournalTestEvent',
        location: 'Terminal',
        context: 'Testing',
        startDate: DateTime.now(),
        infected: false,
        trace: 'testTraceCode',
      );

      eventDB.add(mockEvent);

      final journalPage = _makeTesteableWidget(child: Journal());

      await tester.pumpWidget(journalPage);

      // Verify that the page has an AppBar and a TableCalendar
      expect(find.byType(AppBar), findsOneWidget);
      expect(find.byType(TableCalendar), findsOneWidget);

      // Verify that journal page has todays day selected.
      expect(find.widgetWithText(DecoratedBox, DateTime.now().day.toString()),
          findsOneWidget); // verifies that the widget englobing todays day exists.
      expect(
          ((tester.firstWidget(find.widgetWithText(
                          DecoratedBox, DateTime.now().day.toString()))
                      as DecoratedBox)
                  .decoration as BoxDecoration)
              .color,
          theme
              .accentColor); // Verifies that the widget found before has been selected by looking at the color

      // Verify that _selectedEvent has todays event.
      // TO > Unit testing

      // Verify that the view selection has 'Month' marked so that we see only the week by default.
      expect(find.text('Month'), findsOneWidget);

      // Verify that these buttons are present > refresh, export, about and home button.
      expect(find.byIcon(Icons.refresh), findsOneWidget);
      expect(find.byIcon(Icons.import_export), findsOneWidget);
      expect(find.byIcon(Icons.info), findsOneWidget);
      expect(find.byIcon(Icons.home), findsOneWidget);

      // Verify that the title 'Journal' displays.
      expect(find.text('Journal'), findsOneWidget);

      // Verify that the we see the event by checking if the ListView is present.
      expect(find.byType(ListView), findsOneWidget);

      // Verify that the listTile has an event icon and a coronavirus icon and that it has the coherrect info (name,location,startDate).
      expect(find.byIcon(Icons.event), findsOneWidget);
      expect(find.byIcon(Icons.coronavirus_sharp), findsOneWidget);
      expect(find.text(mockEvent.name), findsOneWidget);
      expect(
          find.text(mockEvent.location +
              " \n" +
              DateFormat.Hm().format(mockEvent.startDate).toString()),
          findsOneWidget);

      // Verify that when the event traceCode changes to 'testTraceCode' the journal updates the event to the state 'infected' after clicking on refresh
      await tester.tap(find.ancestor(
          of: find.byIcon(Icons.refresh),
          matching: find.byType(GestureDetector)));
      await tester.pumpAndSettle();
      expect(
          ((tester.firstWidget(find.byIcon(Icons.coronavirus_sharp)) as Icon)
              .color),
          Colors.red);

      // Verify that the about dialog shows.
      await tester.tap(find.ancestor(
          of: find.byIcon(Icons.info), matching: find.byType(GestureDetector)));
      await tester.pump();
      expect(find.text('Information d\'événement'), findsOneWidget);
      await tester.tap(find.ancestor(
          of: find.text('J\'ai compris'), matching: find.byType(FlatButton)));
      await tester.pumpAndSettle();
      expect(find.text('Information d\'événement'), findsNothing);

      /* List des tests : 
      - Verify that the page has an AppBar and a TableCalendar
      - Verify that journal page has todays day selected.
      - Verify that _selectedEvent has todays event.
      - Verify that the view selection has 'Month' marked so that we see only the week by default.
      - Verify that these buttons are present > refresh, export, about and home button.
      - Verify that the title 'Journal' displays.
      - Verify that the we see the event by checking if the listTile is present.
      - Verify that the listTile has an event icon and a coronavirus icon and that it has the correct info (name,location,startDate).
      - Verify that when the event traceCode changes to 'testTraceCode' the journal updates the event to the state 'infected' after clicking on refresh
      - Verify that the about dialog shows.
       */
    });
  });
}

Widget _makeTesteableWidget({Widget child}) {
  return MaterialApp(
    theme: ThemeData(
      primaryColor: const Color(0xFF05668D), //bleu
      accentColor: const Color(0xFF00A896), //vert
      primaryColorLight: const Color(0xFFF0F3BD), //jaune
      secondaryHeaderColor: const Color(0x9900A896), // vert moins opaque
      fontFamily: 'Gabriela',
    ),
    home: Scaffold(
      body: child,
    ),
  );
}
