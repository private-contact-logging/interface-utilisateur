// This is a basic Flutter widget test.
//
// To perform an interaction with a widget in your test, use the WidgetTester
// utility that Flutter provides. For example, you can send tap and scroll
// gestures. You can also use WidgetTester to find child widgets in the widget
// tree, read text, and verify that the values of widget properties are correct.
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main() async {
  final scannerFinder = find.byValueKey('scan');
  final cancelFinder = find.text('Cancel');

  FlutterDriver driver;

  group('Widgets work as expected', () {
    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('bouton scan est cliquable', () {
      driver.tap(scannerFinder);
    });

    test('Cliquer sur le bouton scan ouvre la caméera', () async {
      await driver.tap(scannerFinder);

      expect(await driver.getText(cancelFinder), "Cancel");
    });
  });
}
