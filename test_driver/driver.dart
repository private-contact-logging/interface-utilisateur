import 'package:naca/main.dart' as app;
import 'package:flutter_driver/driver_extension.dart';

void main() {
  // Enables flutter driver extension
  enableFlutterDriverExtension();

  // Call the `main()` method of our app
  // including all widgets
  app.main();
}
